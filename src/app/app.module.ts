import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { MenuPage } from '../pages/menu/menu';
import { DetailsPage } from '../pages/details/details';
import { NewTaskModalPage } from '../pages/new-task-modal/new-task-modal';
import { NewListModalPage } from '../pages/new-list-modal/new-list-modal';
import { ImagePicker } from '@ionic-native/image-picker';
import { ListDetailsPage } from '../pages/list-details/list-details';
import { ContactsListPage } from '../pages/Contact-List/contacts-list';


import { FirebaseService } from '../pages/services/firebase.service';
import { AuthService } from '../pages/services/auth.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environment/environment';
import { MainTabsPage } from '../pages/main-tabs/main-tabs';
import { AddCampaignPage } from '../pages/add-campaign/add-campaign';
import { from } from 'rxjs';
import { ContactsPage } from '../pages/contacts/contacts';
import { CampaignsPage } from '../pages/campaigns/campaigns';
import { ListsPage } from '../pages/lists/lists';
import { ModelsPage } from '../pages/models/models';
import { InfoPage } from '../pages/info/info';
import { InfoDetailsPage } from '../pages/info-details/info-details';   //ListDetailsPage
import { InfoListPage  } from '../pages/info-list/info-list';     //NewListModalPage


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    RegisterPage,
    MenuPage,
    ListsPage,
    InfoListPage,
    InfoDetailsPage,
    ContactsPage,
    CampaignsPage,
    NewTaskModalPage,
    DetailsPage,
    NewListModalPage,
    ListDetailsPage,
    ModelsPage,
    InfoPage,
    ContactsListPage,
    AddCampaignPage,
    MainTabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RegisterPage,
    MenuPage,
    ListsPage,
    ContactsPage,
    CampaignsPage,
    NewTaskModalPage,
    DetailsPage,
    InfoPage,
    ModelsPage,
    InfoListPage,
    InfoDetailsPage,
    NewListModalPage,
    ListDetailsPage,
    ContactsListPage,
    AddCampaignPage,
    MainTabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ImagePicker,
    FirebaseService,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
