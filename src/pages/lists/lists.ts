import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, List } from 'ionic-angular';
import { FirebaseService } from '../services/firebase.service';
import { NavController, ModalController } from 'ionic-angular';
import { NewListModalPage } from '../new-list-modal/new-list-modal';
import { ListDetailsPage } from '../list-details/list-details';
import { ContactsListPage } from '../Contact-List/contacts-list';
import { Lists } from '../../models/lists.interface';

@Component({
  selector: 'page-lists',
  templateUrl: 'lists.html',
})
export class ListsPage {
  public newLists = [];
  lists: Array<Lists>;
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private firebaseService: FirebaseService,
    private modalCtrl: ModalController,
    private viewCtrl: ViewController,
    ) {
  }

  ionViewDidLoad() {
    this.getData();
  }

  getData(){
    this.firebaseService.getLists()
    .then(lists => {
      this.lists = lists;
     })
  }

  openNewUserModal(){
    let modal = this.modalCtrl.create(NewListModalPage);
    modal.onDidDismiss(data => {
    this.getData();
  });
    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
   }

  onSubmit(value){
    let data = {
      listName: value.listName
    }
    this.firebaseService.createLists(data)
    .then(
      res => {
      }
    )
  }
  viewDetails(id, item){
    //debugger
    let data = {
      title: item.title,
      id: id
    }
    console.log("id:"+ JSON.stringify(id), "item:"+ JSON.stringify(item))
    this.navCtrl.push(ListDetailsPage, {id:id,listName:item.listname})
  }
}
