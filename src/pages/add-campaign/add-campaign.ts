import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Campaign } from '../../models/campaign-list/campaign-list.interface';
import { AngularFireDatabase, AngularFireList  } from 'angularfire2/database';
import { FirebaseService } from '../services/firebase.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ViewController, normalizeURL, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { auth } from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { CampaignsPage } from '../campaigns/campaigns';

@Component({
  selector: 'page-add-campaign',
  templateUrl: 'add-campaign.html',
})
export class AddCampaignPage {
 
 
  campaign={} as Campaign;
  
  campaignData: AngularFireList<Campaign[]>

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private viewCtrl: ViewController,
    private toastCtrl: ToastController,
    private firebaseService: FirebaseService,
    private afAuth: AngularFireAuth,
    private afDatabase: AngularFireDatabase,
     private database: AngularFireDatabase ,   
     private loadingCtrl: LoadingController ) {
        } 

        addCampaign(){
          this.afAuth.authState.take(1).subscribe(auth=>{
            this.afDatabase.list('campaign/auth/uid+/').push(this.campaign)
            
            .then(() => this.navCtrl.push('CampaignsPage'));
          });
          
        }
        
        
}
