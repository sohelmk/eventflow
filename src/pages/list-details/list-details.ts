import { Component } from '@angular/core';
import { ViewController, normalizeURL, ToastController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FirebaseService } from '../services/firebase.service';
import { ImagePicker } from '@ionic-native/image-picker';
import { NavController, ModalController } from 'ionic-angular';
import { ContactsListPage } from '../Contact-List/contacts-list';
import { Lists } from '../../models/lists.interface';

@Component({
  selector: 'page-details',
  templateUrl: 'list-details.html'
})
export class ListDetailsPage {

  validations_form: FormGroup;
  image: any;
  item: any;
  loading: any;
  listName:'';
  constructor(
    private navParams: NavParams,
    private alertCtrl: AlertController,
    private viewCtrl: ViewController,
    private toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private imagePicker: ImagePicker,
    public navCtrl: NavController,
    private firebaseService: FirebaseService,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController
  ) {
    this.listName = navParams.get("listName");
    this.item = this.navParams.get('item');

    // create form using formbuilder
    this.loading = this.loadingCtrl.create();
    console.log("Constructr of :Listdetail"+this.listName);
    this.validations_form = this.formBuilder.group({
      title: new FormControl(this.listName, Validators.required),
    });
//    this.image=this.item.image;
  }
  

  ionViewWillLoad(){
    this.getContactData();
  }

  getContactData(){
  }

  getData(){
  }

  getListsData(){
    }
  
  openContactList(){
    let modal = this.modalCtrl.create(ContactsListPage, {listName:this.listName});
    modal.onDidDismiss(data => {
    this.getData();
  });
    modal.present();
  }

  dismiss() {
   this.viewCtrl.dismiss();
  }

  
  delete() {
    let confirm = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to delete ' + this.item.title + '?',
      buttons: [
        {
          text: 'No',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            this.firebaseService.deleteTask(this.item.id)
            .then(
              res => this.viewCtrl.dismiss(),
              err => console.log(err)
            )
          }
        }
      ]
    });
    confirm.present();
  }

  openImagePicker(){
    this.imagePicker.hasReadPermission()
    .then((result) => {
      if(result == false){
        // no callbacks required as this opens a popup which returns async
        this.imagePicker.requestReadPermission();
      }
      else if(result == true){
        this.imagePicker.getPictures({
          maximumImagesCount: 1
        }).then(
          (results) => {
            for (var i = 0; i < results.length; i++) {
              this.uploadImageToFirebase(results[i]);
            }
          }, (err) => console.log(err)
        );
      }
    }, (err) => {
      console.log(err);
    });
  }

  uploadImageToFirebase(image){
    this.loading.present();
    image = normalizeURL(image);
    let randomId = Math.random().toString(36).substr(2, 5);
    console.log(randomId);

    //uploads img to firebase storage
    this.firebaseService.uploadImage(image, randomId)
    .then(photoURL => {
      this.image = photoURL;
      this.loading.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Image was updated successfully',
        duration: 3000
      });
      toast.present();
    })
  }

}
