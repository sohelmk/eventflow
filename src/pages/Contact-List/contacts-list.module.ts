import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactsListPage } from './contacts-list';
import { ListsPage } from '../lists/lists';

@NgModule({
  declarations: [
    ContactsListPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactsListPage),
  ],
})
export class ContactsPageModule {
  rootPage:any = ListsPage;
}
