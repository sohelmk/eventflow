import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FirebaseService } from '../services/firebase.service';
import { NavController, ModalController } from 'ionic-angular';
import { Contact } from '../../models/contacts.interface';

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts-list.html',
})
export class ContactsListPage {
  contacts: Array<Contact>; //
  isSelected: Array<boolean> = [];
  listName= "";

  constructor(public atrCtrl: AlertController,
    private firebaseService: FirebaseService,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private viewCtrl: ViewController,
    public navParams: NavParams
  ) {
    this.listName = navParams.get("listName");
    console.log(this.listName);
    this.getData();
  }


  ionViewWillEnter() {
    console.log("contacts" + this.contacts.length);
  }

  getData() {
    this.firebaseService.getContacts()
      .then(contacts => {
        this.contacts = contacts;
        for (var _i = 0; _i < contacts.length; _i++) {
          var num = contacts[_i];
          this.isSelected[_i] = false;
        }
      })

  }

  getData1(){
    this.firebaseService.getContacts()
    .then(contact => {
      this.contacts = contact;
    })
  }


  dismiss() {
    this.viewCtrl.dismiss();
   }

  submit() {
    for (var _i = 0; _i < this.isSelected.length; _i++) {
      if (this.isSelected[_i]) {
        //add that contact to list
        console.log(" listName" + this.listName);
        console.log("adding contact number:" + _i);
        console.log(" contact.firstname" + this.contacts[_i].payload.doc.data().firstname);
        this.firebaseService.updatelistcontacts(this.contacts[_i].payload.doc.data(), this.listName)
        .then(
        res => {
        this.viewCtrl.dismiss();
        }
        )
      }
      
      else
        console.log("not adding contact number:" + _i);

    }
  }
}