import { Component } from '@angular/core';
import { ViewController, normalizeURL, ToastController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FirebaseService } from '../services/firebase.service';
import { ImagePicker } from '@ionic-native/image-picker';

////
@Component({
  selector: 'page-models',
  templateUrl: 'models.html'
})
export class ModelsPage {

  validations_form: FormGroup;
  image: any;
  loading: any;
 
  constructor(
    private navParams: NavParams,
    private alertCtrl: AlertController,
    private viewCtrl: ViewController,
    private toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private imagePicker: ImagePicker,
    private firebaseService: FirebaseService,
    private loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }

  ionViewWillLoad(){
    this.resetFields();
  }
  

  resetFields(){
    this.image = "./assets/imgs/default_image.jpg";
    this.validations_form = this.formBuilder.group({
      campaignname: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
  }
  
  dismiss() {
   this.viewCtrl.dismiss();
  }

  onSubmit(value){
    let data = {
      campaignname: value.campaignname,
      description: value.description,
      
    }
    this.firebaseService.createCampaign(data)
    .then(
      res => {
        this.resetFields();
        this.viewCtrl.dismiss();
      }
    )
  }
}

 
 