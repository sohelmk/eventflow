import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FirebaseService } from '../services/firebase.service';
import { NewTaskModalPage } from '../new-task-modal/new-task-modal';
import { NavController, ModalController } from 'ionic-angular';
import { DetailsPage } from '../details/details';
import { Contact } from '../../models/contacts.interface';

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {
  public newContact = [];
  contacts: Array<Contact>;
  
  constructor(public atrCtrl: AlertController, private firebaseService: FirebaseService,
    private modalCtrl: ModalController, private navCtrl: NavController,

    ) {

  }


  ionViewWillEnter(){
    this.getData();
  }

  getData(){
    this.firebaseService.getContacts()
    .then(contact => {
      this.contacts = contact;
    })
  }
  openNewUserModal(){
    let modal = this.modalCtrl.create(NewTaskModalPage);
    modal.onDidDismiss(data => {
    this.getData();
  });
    modal.present();
  }
  
  onSubmit(value){
    let data = {
      firstname: value.firstname,
      lastname: value.lastname,
      email: value.email
    }
    this.firebaseService.createContact(data)
    .then(
      res => {
      }
    )
  }
  viewDetails(id, item){
    // debugger
    let data = {
      title: item.firstname,
      discription: item.lastname,
      email: item.email,
      image: item.image,
      id: id
    }
    this.navCtrl.push(DetailsPage, {id:id,item:item})
    

  }
  }
