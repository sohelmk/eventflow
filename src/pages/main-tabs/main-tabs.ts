import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ContactsPage } from '../contacts/contacts';
import { CampaignsPage } from '../campaigns/campaigns';
import { ListsPage } from '../lists/lists';


@IonicPage()
@Component({
  selector: 'page-main-tabs',
  templateUrl: 'main-tabs.html'
})
export class MainTabsPage {

  contactsRoot = ContactsPage;
  campaignsRoot = CampaignsPage;
  listsRoot = ListsPage


  constructor(public navCtrl: NavController) {}

}
