import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { FirebaseService } from '../services/firebase.service';
import { NavController, ModalController } from 'ionic-angular';
import { InfoDetailsPage } from '../info-details/info-details';   //ListDetailsPage
import { InfoListPage  } from '../info-list/info-list';     //NewListModalPage
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  public newLists = [];
  lists: Array<any>;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private firebaseService: FirebaseService,
    private modalCtrl: ModalController,
    private viewCtrl: ViewController,
    ) {
  }

  ionViewDidLoad() {
    this.getData();
  }

  getData(){
    this.firebaseService.getLists()
    .then(lists => {
      this.lists = lists;
     })
  }

  openNewUserModal(){
    let modal = this.modalCtrl.create(InfoListPage);
    modal.onDidDismiss(data => {
    this.getData();
  });
    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
   }

  onSubmit(value){
    let data = {
      listname: value.listname
    }
    this.firebaseService.createLists(data)
    .then(
      res => {
      }
    )
  }
  viewDetails(id, item){
    // debugger
    let data = {
      title: item.title,
      discription: item.discription,
      id: id
    }
    this.navCtrl.push(InfoDetailsPage, {id:id,item:item})

  }
}
