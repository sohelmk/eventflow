import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FirebaseService } from '../services/firebase.service';
import { ModelsPage } from '../models/models';
import { InfoPage } from '../info/info';


@IonicPage()
@Component({
  selector: 'page-campaigns',
  templateUrl: 'campaigns.html',
})
export class CampaignsPage {
  public newCampaign = [];
  campaigns: Array<any>;

  constructor(public atrCtrl: AlertController, private firebaseService: FirebaseService,
    private modalCtrl: ModalController, private navCtrl: NavController,

       ) {
  }
  
  ionViewWillEnter(){
    this.getData();
  }
  getData(){
    this.firebaseService.getCampaigns()
    .then(campaigns => {
      this.campaigns =campaigns;
    })
  }
  openNewUserModal(){
    let modal = this.modalCtrl.create(ModelsPage);
    modal.onDidDismiss(data => {
      this.getData();
    });
    modal.present();
  }
  onSubmit(value){
    let data = {
      campaignname: value.campaignname,
      description: value.description,
      
    }
    this.firebaseService.createCampaign(data)
    .then(
      res => {
      }
    )
  }
  viewDetails(id, item){
    // debugger
    let data = {
      title: item.campaignname,
      
      descriptions: item.description,
      image: item.image,
      id: id
    }
    this.navCtrl.push(InfoPage, {
      id:id,item:item
    })
  }
  }
